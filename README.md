# NYC POPS

A map of Privately Owned Public Spaces in New York City, with some basic filtering. (nyc-pops.herokuapp.com)

## Background

This repository contains a JSON representation of POPS in New York City. This is a combination of data from [NYC Open Data](https://data.cityofnewyork.us/Housing-Development/Privately-Owned-Public-Spaces/fum3-ejky) and [Advocates for Privately Owned Public Spaces](https://apops.mas.org). APOPS has a lot of useful information, but I found their map view lacking. I adapted a previous Chrome extension in order to scrape APOPS info like amenities, and then merged the data sets.

## Deploying
Everything you need is in `public_html/`. There is also `Procfile`, `composer.json`, and `public_html/index.php` which allows deployment on Heroku.

## TODO

* Make more mobile friendly
* Clean up repo (move/remove files as necessary)
* Add License
