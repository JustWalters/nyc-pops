jQuery(() => {
	let map = new google.maps.Map(document.getElementById('map'), {
		zoom: 12,
		center: new google.maps.LatLng(40.7406017, -73.9776556),
		mapTypeId: 'terrain'
	}),
		form = $('#filters-form'),
		markers = [],
		markerCluster;

	$('#toggle-form-btn').click(toggleForm);
	$('#update-btn').click(updateFilters);
	$.getJSON('tbl_POPS.json', withData);

	function withData(result) {
		if (markers.length || !map) return;

		const displayProps = ['Building Address', 'Building Location', 'Building Name', 'Community District', 'Year Completed', 'Public Space 1', 'Public Space 2', 'Public Space 3', 'Public Space 4', 'Public Space 5'];
		const topExtra = {'hours': 'Hours', /*'bookProfile': 'Profile',*/ 'description': 'Description'};
		const topExtraKeys = Object.keys(topExtra);
		const moreExtra = ['amenities', 'generalInfo'];
		const apopsBase = 'https://apops.mas.org/pops/';
		const formLinkBase = 'https://docs.google.com/forms/d/e/1FAIpQLSfc21p2-QW-k5fVIpJwhGurz2YcHm7Is-DpnQ_0Ube6gJZnWQ/viewform?usp=pp_url&entry.2124538334&entry.1294512401';

		result.forEach(function(datum) {
			let latLng, title, content, marker, cb;

			latLng = new google.maps.LatLng(datum.Lat, datum.Lng);
			title = `${datum['Building Address']}`;
			if (datum['Building Name']) title += ` - ${datum['Building Name']}`;

			content = `<h3>${title}</h3>`;
			displayProps.forEach(function(prop) {
				if (datum[prop]) {
					content += `<br><b>${prop}</b>: ${datum[prop]}`;
				}
			});

			if (datum.extra) {
				let extra = datum.extra;
				topExtraKeys.forEach(function(prop) {
					if (extra[prop]) {
						content += `<br><b>${topExtra[prop]}</b>: ${extra[prop]}`;
					}
				});

				content += `<br><br><a href="${apopsBase + datum['DCP RECORD']}" target="_blank">For more info, visit APOPS@MAS</a>`;
			}

			formLink = `${formLinkBase}&entry.1722611083=${encodeURIComponent(datum['Building Address'])}`;
			content += `<br><a href="${formLink}" target="_blank">Incorrect info?</a>`;

			infowindow = new google.maps.InfoWindow({
				content
			});

			marker = new google.maps.Marker({
				position: latLng,
				map,
				title,
				info: datum
			});

			markers.push(marker);

			cb = (_infowindow) => () => { _infowindow.open(map, marker); };

			google.maps.event.addListener(marker, 'click', cb(infowindow));
		});

		markerCluster = new MarkerClusterer(map, markers,
			{ imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
		markerCluster.setIgnoreHidden(true);
	}

	function updateFilters(e) {
		e.preventDefault();
		toggleForm();
		let alwaysOpen = $('#always-open').prop('checked');
		let district = $('#community-district').val();
		let fountain = $('#drinking-fountain').prop('checked');
		let food = $('#food-service').prop('checked');
		let trash = $('#litter-receptacles').prop('checked');
		let planting = $('#planting').prop('checked');
		let seating = $('#seating').prop('checked');
		let treesOnStreet = $('#trees-on-street').prop('checked');
		let treeInSpace = $('#trees-within-space').prop('checked');
		let tables = $('#tables').prop('checked');
		let water = $('#water-feature').prop('checked');
		let retail = $('#retail-frontage').prop('checked');
		let climate = $('#climate-control').prop('checked');
		let programs = $('#programs').prop('checked');
		let bicycle = $('#bicycle-parking').prop('checked');
		let artwork = $('#artwork').prop('checked');
		let escalator = $('#escalator-elevator').prop('checked');
		let restrooms = $('#restrooms').prop('checked');
		let subway = $('#subway').prop('checked');
		let filters = {
			alwaysOpen,
			district,
			fountain,
			food,
			trash,
			planting,
			seating,
			treesOnStreet,
			treeInSpace,
			tables,
			water,
			retail,
			climate,
			programs,
			bicycle,
			artwork,
			escalator,
			restrooms,
			subway
		};

		runFilters(filters);
		markerCluster.repaint();
	}

	function matchesFilters(marker, filters) {
		// TODO: return true when no filters applied, false otherwise
		if (filters.district && filters.district !== marker['Community District']) return false;

		if (Object.values(filters).every(x => !x)) return true;
		let amenities;
		if (!marker.extra || !marker.extra.amenities) amenities = {};
		else amenities = marker.extra.amenities;

		let isInAmenities = (key) => !!amenities[key];
		let isAlwaysOpen = () => marker.extra && marker.extra.hours && marker.extra.hours.indexOf('24 hours') > -1;

		if (filters.alwaysOpen && !isAlwaysOpen()) return false;
		if (filters.fountain && !isInAmenities('Drinking Fountain')) return false;
		if (filters.food && !isInAmenities('Food Service')) return false;
		if (filters.trash && !isInAmenities('Litter Receptacles')) return false;
		if (filters.planting && !isInAmenities('Planting')) return false;
		if (filters.seating && !isInAmenities('Seating')) return false;
		if (filters.treesOnStreet && !isInAmenities('Trees on Street')) return false;
		if (filters.treeInSpace && !isInAmenities('Trees within Space')) return false;
		if (filters.tables && !isInAmenities('Tables')) return false;
		if (filters.water && !isInAmenities('Water Feature')) return false;
		if (filters.retail && !isInAmenities('Retail Frontage')) return false;
		if (filters.climate && !isInAmenities('Climate Control')) return false;
		if (filters.programs && !isInAmenities('Programs')) return false;
		if (filters.bicycle && !isInAmenities('Bicycle Parking')) return false;
		if (filters.artwork && !isInAmenities('Artwork')) return false;
		if (filters.escalator && !isInAmenities('Escalator/Elevator')) return false;
		if (filters.restrooms && !isInAmenities('Restrooms')) return false;
		if (filters.subway && !isInAmenities('Subway')) return false;

		return true;
	}

	function runFilters(filters) {
		markers.forEach(function(marker) {
			marker.setVisible(matchesFilters(marker.info, filters));
		});
	}

	function toggleForm() {
		form.toggle();
	}
});
